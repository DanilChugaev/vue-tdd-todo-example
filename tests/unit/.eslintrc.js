/* global module */

module.exports = {
  env: {
    mocha: true
  },
  rules: {
    'import/no-extraneous-dependencies': 'off'
  }
}